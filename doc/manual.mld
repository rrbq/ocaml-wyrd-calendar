{0 Wyrd User Manual}
Paul J. Pelzl

{e "Because you're tired of waiting for your bloated calendar program to start up."}

{%html:<nav class="odoc-nav"><a href="wyrd.html">wyrd command reference</a></nav>%}
{%html:<nav class="odoc-nav"><a href="wyrdrc.html">wyrdrc syntax</a></nav>%}

{1 Introduction}
Wyrd is a text-based front-end to {{:https://dianne.skoll.ca/projects/remind} Remind}, a sophisticated calendar and alarm program.
Wyrd serves two purposes:

+ It displays reminders in a scrollable timetable view suitable for visualizing your calendar at a glance.
+ It makes creating and editing reminders fast and easy.
  However, Wyrd does not hide Remind's textfile programmability, for this is what makes Remind a truly powerful calendaring system.

Wyrd also requires only a fraction of the resources of most calendar programs available today.


{1 Installation}
This section describes how to install Wyrd by compiling from source.
Wyrd has been packaged for a number of popular Linux/Unix variants, so you may be able to save yourself some time by installing from a package provided by your OS distribution.

Wyrd is designed to be portable to most Unix-like operating systems, including GNU/Linux, *BSD, and Mac OS X.
Before installing Wyrd, your system must have the following software installed:

- {{:https://ocaml.org/} OCaml} >= 4.08
- the {{:https://invisible-island.net/ncurses/} ncurses library} (and development headers)
- {{:https://github.com/mbacarella/curses} OCaml Curses}
- {{:https://dianne.skoll.ca/projects/remind/} Remind} >= 4.0.0
- {{:https://www.gnu.org/software/make/} GNU make}
- the Unix pager application, [less]

After installing the OCaml compiler, Wyrd may be compiled and installed through any of the following 3 methods:

{2 Method 1: GNU make and OCaml findlib}

+ Install the OCaml findlib package for your operating system: https://repology.org/project/findlib/related
+ Install the OCaml curses package for your operating system: https://repology.org/project/ocaml:curses/versions
+ Execute the following commands at the root of the source tree:
{v
   export PREFIX=/usr
   make
   make install
v}
On the BSDs, GNU make is usually available under the name [gmake] instead of [make].

{2 Method 2: Dune}

+ Install the Dune package for your operating system: https://repology.org/project/dune/versions
+ Install the OCaml curses package for your operating system: https://repology.org/project/ocaml:curses/versions
+ Execute the following commands at the root of the source tree:
{v
   export WYRD_SYSCONFDIR=/etc
   dune build --release
   dune install --prefix=/usr --sections=bin
v}

{2 Method 3: OPAM}

+ Install the OPAM package for your operating system: https://opam.ocaml.org/doc/Install.html
+ Execute the following commands at the root of the source tree:
{v
   opam init -c ocaml-system -n
   opam install -y ./wyrd.opam
v}
+ Wyrd may then be started with this command:
{v
   opam exec wyrd
v}


{1 Quick Start}

This section describes how to use Wyrd in its default configuration.
After familiarizing yourself with the basic operations as outlined in this section, you may wish to consult the {{!page-wyrdrc} Advanced Configuration} section to see how Wyrd can be configured to better fit your needs.

{1 Overview}
Before attempting to use Wyrd, learn how to use Remind.
Wyrd makes no attempt to hide the details of Remind programming from the user.
Aside from reading the Remind manpage, you may get some useful pointers by reading {{:https://www.43folders.com/2005/02/24/guest-mike-harris-looks-at-remind} Mike Harris's article on 43 Folders}, {{:https://www.linuxjournal.com/article/3529} Dianne Skoll's writeup on Linux Journal}, or {{:https://dianne.skoll.ca/wiki/Remind} the Remind Wiki}.

You can launch Wyrd using the default reminder file by executing [wyrd].
If desired, a different reminder file (or reminder directory) may be selected by executing [wyrd <filename>].

At the top of the window is a short (incomplete) list of keybindings.

The left window displays a scrollable timetable view, with reminders highlighted in various colors.
If the [DURATION] specifier is used for a reminder, the highlighted area is rendered with an appropriate size.
Overlapping reminders are rendered using one of four different indentation levels so that all reminders are at least partially visible.
If the current time is visible in this window, it is highlighted in red.

The upper right window displays a month calendar, with the color of each day representing the number of reminders it contains.
The colors range across shades of white to blue to magenta as the number of reminders increases.
The selected date is highlighted in cyan; if the current date is visible, it is highlighted in red.

The lower right window displays a list of the untimed reminders falling on the selected date.

The bottom window displays the full text of the [MSG] for the reminder or reminders that are currently selected.

{1 Navigation}

{%html:<div>
<table><tr class="center"><td>Action</td><td>Keypress</td></tr>
<tr><td>scroll up and down the schedule</td><td>&lt;up&gt;, &lt;down&gt; or k, j</td></tr>
<tr><td>jump back or forward by a day</td><td>&lt;pageup&gt;, &lt;pagedown&gt; or 4, 6 or &lt;, &gt; or H, L</td></tr>
<tr><td>jump back or forward by a week</td><td>8, 2 or [, ] or K, J</td></tr>
<tr><td>jump back or forward by a month</td><td>{, }</td></tr>
<tr><td>jump to current date and time</td><td>&lt;home&gt;</td></tr>
<tr><td>jump to the next reminder</td><td>&lt;tab&gt;</td></tr>
<tr><td>switch between schedule and untimed reminders window</td><td>&lt;left&gt;, &lt;right&gt; or h, l</td></tr>
<tr><td>zoom in on the schedule</td><td>z</td></tr>
<tr><td>scroll the description window up and down</td><td>d, D</td></tr>
</table>
</div>%}
{%latex:
\begin{center}
   \begin{tabular}[t]{*{2}{p{.4\textwidth}}}
      \toprule
      Action & Keypress \\
      \midrule
      scroll up and down the schedule & \texttt{<up>}, \texttt{<down>} or \texttt{k}, \texttt{j} \\
      jump back or forward by a day & \texttt{<pageup>}, \texttt{<pagedown>} or \texttt{4}, \texttt{6} or \texttt{<}, \texttt{>} or \texttt{H}, \texttt{L} \\
      jump back or forward by a week & \texttt{8}, \texttt{2} or \texttt{[}, \texttt{]} or \texttt{K}, \texttt{J} \\
      jump back or forward by a month & \texttt{\{}, \texttt{\}} \\
      jump to current date and time & \texttt{<home>} \\
      jump to the next reminder & \texttt{<tab>} \\
      switch between schedule and untimed reminders window & \texttt{<left>}, \texttt{<right>} or \texttt{h}, \texttt{l} \\
      zoom in on the schedule & \texttt{z} \\
      scroll the description window up and down & \texttt{d}, \texttt{D}
      \\
      \bottomrule
   \end{tabular}
\end{center}

%}
{%manpage:
.TS
tab(&);
l l.
T{
T}
_
T{
Action
T}&T{
Keypress
T}
_
T{
scroll up and down the schedule
T}&T{
<up>,
<down>
or k,
j
T}
T{
jump back or forward by a day
T}&T{
<pageup>,
<pagedown>
or 4,
6
or <,
>
or H,
L
T}
T{
jump back or forward by a week
T}&T{
8,
2
or [,
]
or K,
J
T}
T{
jump back or forward by a month
T}&T{
{,
}
T}
T{
jump to current date and time
T}&T{
<home>
T}
T{
jump to the next reminder
T}&T{
<tab>
T}
T{
switch between schedule and untimed reminders window
T}&T{
<left>,
<right>
or h,
l
T}
T{
zoom in on the schedule
T}&T{
z
T}
T{
scroll the description window up and down
T}&T{
d,
D
T}
_
.TE
%}


Notice that if you have a numeric keypad, the [{4, 6, 8, 2}] keys will let you move directionally in the month calendar view at the upper-right of the screen.
Similarly, [{H, J, K, L}] will cause directional calendar movement using the standard mapping from [vi(1)].

In addition to the hotkeys provided above, Wyrd lets you jump immediately to a desired date by pressing ['g'], entering in a date specifier, and then pressing [<return>].
Any of the following date specifiers may be used:

- 8 digits representing year, month, and day: YYYYMMDD
- 4 digits representing month and day (of current year): MMDD
- 2 digits representing day (of current month and year): DD

(The date specifier format may be changed to DDMMYYYY; consult the section on {{!page-wyrdrc} Configuration Variables}.)

{1 Editing Reminders}
Note: By default, Wyrd is configured to modify your reminder files using the text editor specified by the [$EDITOR] environment variable.
(This configuration has been tested successfully with a number of common settings for [$EDITOR], including ['vim'], ['emacs'], and ['nano'].)
If you wish to use a different editor, see {{!page-wyrdrc} Advanced Configuration}.

If you select a timeslot in the schedule view, then hit ['t'], you will begin creating a new timed reminder.
Wyrd will open up your reminder file in your favorite editor and move the cursor to the end of the file, where a new reminder template has been created.
The template has the selected date and time filled in, so in many cases you will only need to fill in a [MSG] value.

Similarly, hitting ['u'] will begin creating an untimed reminder.
['w'] will create a weekly timed reminder, and ['W'] will create a weekly untimed reminder; ['m'] will create a monthly timed reminder, and ['M'] will create a monthly untimed reminder.

The keys ['T'] and ['U'] also create timed and untimed reminders (respectively), but first will provide a selection dialog for you to choose which reminder file you want to add this reminder to.
The set of reminder files is determined by scanning the [INCLUDE] lines in your default reminder file.
(If you use a reminder directory, then all [*.rem] files in that directory will be available along with all [INCLUDE]d files.)

If you select a reminder (either timed or untimed) and hit [<return>], you will begin editing that reminder.
Wyrd will open up the appropriate reminders file in your editor and move the cursor to the corresponding [REM] line.

If you select a timeslot that contains multiple overlapping reminders, Wyrd will provide a dialog that allows you to select the desired reminder.

If you hit [<enter>] on a blank timeslot, Wyrd will begin creating a new timed or untimed reminder (depending on whether the timed or the untimed window is selected).

Finally, pressing ['e'] will open the reminder file in your editor without attempting to select any particular reminder.

{1 Quick Reminders}
Wyrd offers an additional mode for entering simple reminders quickly.
Press ['q'], and you will be prompted for an event description.
Simply enter a description for the event using natural language, then press [<return>].
Examples:

- meeting with Bob tomorrow at 11
- drop off package at 3pm
- Wednesday 10am-11:30 go grocery shopping
- Board game night 20:15 next Fri
- 7/4 independence day
- 7/4/2007 independence day (next year)
- independence day (next year) on 2007-07-04

If your event description can be understood, Wyrd will immediately create the reminder and scroll the display to its location.

Currently the quick reminder mode tends to favor USA English conventions, as generalizing the natural language parser would require some work.

Wyrd also allows you to use the "quick reminder" syntax to create new reminders from the command line, using the [-a] or [--add] options.
For example,
{v
   wyrd --add "dinner with neighbors tomorrow at 7pm"
v}
would create a new reminder for tomorrow evening.
When used in this mode, Wyrd exits silently with error code 0 if the reminder was successfully created.
If the reminder could not be created (e.g. if the reminder syntax could not be parsed), Wyrd prints an error message and exits with a nonzero error code.

{1 Cutting and Pasting Reminders}
Reminders can be easily duplicated or rescheduled through the use of Wyrd's cutting and pasting features.

Selecting a reminder and pressing ['X'] will cut that reminder: the corresponding [REM] line is deleted from your reminders file, and the reminder is copied to Wyrd's clipboard.
To copy a reminder without deleting it, use ['y'] instead.

To paste a reminder from the clipboard back into your schedule, just move the cursor to the desired date/time and press ['p'].
Wyrd will append a new [REM] line to the end of your reminders file, and open the file with your editor.
The [REM] line will be configured to trigger on the selected date.
If the copied reminder was timed, then the pasted reminder will be set to trigger at the selected time using the original [DURATION] setting.
(Additional Remind settings such as [delta] and [tdelta] are not preserved by copy-and-paste.)

If you wish to paste a reminder into a non-default reminders file, use ['P'].
This will spawn a selection dialog where you can choose the file that will hold the new reminder.

{b WARNING}: Cutting a reminder will delete only the single [REM] command responsible for triggering it.
If you are using more complicated Remind scripting techniques to generate a particular reminder, then the [cut] operation may not do what you want.

{1 Viewing Reminders}

Aside from viewing reminders as they fall in the schedule, you can press ['r'] to view all reminders triggered on the selected date in a [less(1)] window.
Similarly, ['R'] will view all reminders triggered on or after the selected date (all non-expired reminders are triggered).

If you want to get a more global view of your schedule, Wyrd will also let you view Remind's formatted calendar output in a [less(1)] window.
Pressing ['c'] will view a one-week calendar that contains the selected date, while pressing ['C'] will view a one-month calendar containing the selected date.

{1 Searching for Reminders}

Wyrd allows you to search for reminders with [MSG] values that match a search string.
Press ['/'] to start entering a (case insensitive) regular expression.
After the expression has been entered, press [<return>] and Wyrd will locate the next reminder that matches the regexp.
Press ['n'] to repeat the same search.
Entry of a search string may be cancelled with [<esc>] or [Ctrl-g].

The regular expression syntax is Emacs-compatible.

Note: Sorry, there is no "search backward" function.
The search function requires the use of ["remind -n"], which operates only forward in time.
For the same reason, there is a command to jump forward to the next reminder, but no command to jump backward to the previous reminder.

{1 Other Commands}

A list of all keybindings may be viewed by pressing ['?'].
You can exit Wyrd by pressing ['Q'].
If the screen is corrupted for some reason, hit ['Ctrl-L'] to refresh the display.

{1 Alarm Strategies}
You may wish to generate some sort of alarm when a reminder is triggered.
Wyrd does not offer any special alarm functionality, because Remind can handle the job already.
Check the Remind manpage and consider how the [-k] option could be used to generate alarms with the aid of external programs.
For example, the following command will generate a popup window using [gxmessage(1)] whenever a timed reminder is triggered:
{v
   remind -z -k'gxmessage -title "reminder" %s &' ~/.reminders &
v}
(A sensible way to start this alarm command is to place it in [~/.xinitrc] so that it launches when the X server is started.)

If you want some advance warning (say, 15 minutes), you can cause Remind to trigger early by setting a [tdelta] in the AT clause:
{v
   REM Nov 27 2005 AT 14:30 +15 MSG Do something
v}

Alternatively, if you want to generate alarms only for specific reminders, consider using Remind's [RUN] command.
This process could be easily automated by using the [templateN] configuration variables described in {{!page-wyrdrc} Advanced Configuration}.

{1 Miscellaneous}

Remind's [TAG] specifier may be used to cause Wyrd to give special treatment to certain reminders.
If a reminder line includes the clause ["TAG noweight"], then Wyrd will not give that reminder any weight when determining the "busy level" colorations applied to the month calendar.
If a reminder line includes the clause ["TAG nodisplay"], then Wyrd will neither display that reminder nor give it any weight when determining the month calendar colorations.
The tag parameters are case insensitive.

{b WARNING}: These tag parameters are not guaranteed to interact well with other Remind front-ends such as tkremind.


{1 Usage Tips}
Wyrd fills in sensible defaults for the fields of a [REM] statement, but you will inevitably need to make some small edits to achieve the behavior you want.
If you use Vim, you can make your life easier by installing the Vim-Latex Suite and then modifying your [~/.wyrdrc] to use [REM] templates like this:
{v
   set timed_template="REM %monname% %mday% %year% <++>AT %hour%:%min%<++> \
      DURATION 1:00<++> MSG %\"<++>%\" %b"
   set untimed_template="REM %monname% %mday% %year% <++>MSG %\"<++>%\" %b"
v}
With this change, hitting Ctrl-J inside Vim (in insert mode) will cause your cursor to jump directly to the [<++>] markers, enabling you to quickly add any desired Remind delta and message parameters.

There is a {{:https://dianne.skoll.ca/wiki/Wyrd} page on Wyrd} in the Remind Wiki.
This is a good place to look for other usage tips.


{1 Licensing}
SPDX-License-Identifier: [GPL-2.0-only]

Wyrd is Free Software; you can redistribute it and/or modify it under the terms of the GNU General Public License (GPL), Version 2, as published by the Free Software Foundation.
You should have received a copy of the GPL along with this program, in the file ['COPYING'].


{1 Acknowledgments}
Thanks, of course, to Dianne Skoll for writing such a powerful reminder system.

Thanks also to Nicolas George, who wrote the OCaml curses bindings used within Wyrd.

{1 Contact info}
{%html:
Wyrd website: <a href="https://gitlab.com/wyrd-calendar/wyrd">https://gitlab.com/wyrd-calendar/wyrd</a> %}
{%latex:
Wyrd website: \href{https://gitlab.com/wyrd-calendar/wyrd}{https://gitlab.com/wyrd-calendar/wyrd} %}
{%manpage:
Wyrd website: https://gitlab\.com/wyrd-calendar/wyrd %}

{1 Miscellaneous}
"Wyrd is a concept in ancient Anglo-saxon and Nordic cultures roughly corresponding to fate or personal destiny."  {e -- Wikipedia}
