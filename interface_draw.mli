(*  Wyrd -- a curses-based front-end for Remind
 *  Copyright (C) 2005, 2006, 2007, 2008, 2010, 2011-2013 Paul Pelzl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License, Version 2,
 *  as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *)


val sort_lineinfo : Interface.timed_lineinfo_t -> Interface.timed_lineinfo_t -> int

val draw_help : Interface.interface_state_t -> unit

val draw_date_strip : Interface.interface_state_t -> unit

val draw_timed :
  Interface.interface_state_t -> Remind.timed_rem_t list array -> Interface.interface_state_t

val draw_timed_try_window :
  Interface.interface_state_t -> Remind.timed_rem_t list array -> int -> int
  -> Interface.interface_state_t

val draw_calendar : Interface.interface_state_t -> Remind.three_month_rem_t -> unit

val draw_untimed :
  Interface.interface_state_t -> Remind.untimed_rem_t list -> Interface.interface_state_t

val draw_msg : Interface.interface_state_t -> Interface.interface_state_t

val draw_error : Interface.interface_state_t -> string -> bool -> unit

val draw_selection_dialog :
  Interface.interface_state_t -> string -> string list -> int -> int -> unit
